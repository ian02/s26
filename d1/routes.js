const http = require("http");

//Create a variable "port" to store the port number
const port = 4000;

//Create a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {
	//Accessing the 'greeting' route that returns a message of "Hello Again"
	//http://localhost:4000/greeting
	//"request" is an object that is sent via the client (browser)
	//The "url" property refers to the url or the link in the browser
	if(request.url == '/greeting'){
		response.writeHead (200, {'Content-Type': 'text/plain'})
		response.end ('Hello Again')
	}

	else if(request.url == '/homepage'){
		response.writeHead (200, {'Content-Type': 'text/plain'})
		response.end ('This is the homepage')
	}
	else {
		response.writeHead (404, {'Content-Type': 'text/plain'})
		response.end ('Page not available')
	}
})

//Mini Activity: ^
//Access the "homepage" route and return a message of "This is the homepage" with a status code 200 and a plain text.
//All other routes will return a message of "page not available".  Give a status code for not found and plain text.


server.listen(port);

//When server is running, console will print the message:
console.log(`Server now accessible at localhost:${port}`);

//sudo npm install nodemon -g    
	//install in terminal

//nodemon routes.js 
	//write in terminal

