//Use the 'require' directive to load Node js modules
//A 'module' is a software component or part of a program that contains one or more routines

let http = require("http");

//The 'http module' can transfer data using Hypertext Transfer Protocol
//it is a ser of individual files that contain code to create a "component" that helps establish a data transfer between applications
//HTTP is a protocol that allows the fetching of resources such as HTML documents.
//Client(browser) and server (nodeJS) communicate by exchanging individual messages.
//The messages sent by the client, usually a Web browser, are called "requests".
//The messages sent by the server as an answer from the request are called "responses".

//Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified address/URL/port and gives responses back to the client

//A port is a virtual point where network connections start and end.
//Each port is associated with a specific process or service
//http://localhost:4000
//The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to our server.

http.createServer(function(request, response) {

	//we use the writeHead () method to:
		//Set a status code for the response - a 200 means OK
		//Set the content-type of the response as a plain text message
		//value of content type can be image/jpeg or application/json
	response.writeHead(200, {'Content-Type': 'text/plain'});
	//Send the response with text content "Hello World"
	//We used the response.end () method to end the response process (closed the connection)
	response.end("Hello World")
}).listen(4000)

console.log('Server running at localhost:4000');

// node index.js
// Ctrl + C in terminal to stop the server












